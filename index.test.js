const checkWord = require('./index.js');

test('Word is valid', () => {
    expect(checkWord('AAAAABBBB')).toBe(true);
    expect(checkWord('AAAAA')).toBe(true);
    expect(checkWord('BBBB')).toBe(true);
    expect(checkWord('BBBABBABB')).toBe(false);
    expect(checkWord('BBBLABBMABB')).toBe(false);
})
